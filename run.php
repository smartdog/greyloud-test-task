<?php
require_once "lib.php";

if (getopt('h') || getopt('help')) {
    echo "Options:".PHP_EOL;
    echo "  -h -- help".PHP_EOL;
    echo "  -f <file_name> -- data file".PHP_EOL;
    exit(0);
}

$tmp = getopt("f:");
if (empty($tmp)) {
    echo '(W) Missing filename';
    exit(0);
} else {
    $fname = $tmp['f'];
}

$fpath = sprintf('%s', $fname);
if (!file_exists($fpath)) {
    echo '(E) File not found';
    exit(0);
}

$file = fopen($fpath, 'r');
if (!$file) {
    echo '(E) File opening error';
    exit(0);
}

$result = maxLakeDepthFile($file);
fclose($file);

printf("Max depth: %s".PHP_EOL, $result['depth']);
printf("Max depth index: %s".PHP_EOL, $result['index']);
