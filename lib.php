<?php

/**
 * @param resource $file
 * @return array
 */
function maxLakeDepthFile($file): array
{
    $continueFlag = false;
    $index = 1;
    $depth = 0;
    $depthIndex = null;
    $fileOffset = 0;
    $maxNext = null;
    $maxNextIndex = null;
    do {
        $maxFirst = null;
        $maxFirstIndex = null;
        if ($continueFlag) {
            $index = $maxNextIndex+1;
            fseek($file, $fileOffset);
            $continueFlag = false;
            $maxFirst = $maxNext;
            $maxFirstIndex = $maxNextIndex;
            $maxNext = null;
            $maxNextIndex = null;
        }
        $min = null;
        $minIndex = null;
        $minNext = null;
        $minNextIndex = null;

        while (!feof($file)) {
            $tmp = fgets($file);
            if (!is_string($tmp)) {
                continue;
            } else {
                $value = (int)$tmp;
            }

            if ($maxFirst === null) {
                $maxFirst = $value;
                $maxFirstIndex = $index;
            } elseif ($maxFirst <= $value && $maxFirstIndex == $index - 1) {
                $maxFirst = $value;
                $maxFirstIndex = $index;
            } elseif (($maxFirst > $value && $min === null) || ($min > $value && $maxNext == null)) {
                $min = $value;
                $minIndex = $index;
            } elseif ($min > $value && ($minNext === null || $minNext > $value)) {
                $minNext = $value;
                $minNextIndex = $index;
            } elseif ($min < $value && ($maxNext === null || $maxNext <= $value)) {
                $maxNext = $value;
                $maxNextIndex = $index;
                if ($minNext !== null) {
                    $min = $minNext;
                    $minIndex = $minNextIndex;
                    $minNext = null;
                    $minNextIndex = null;
                }
                $continueFlag = false;
                $fileOffset = ftell($file);
            }

            if ($continueFlag === false && $index > $maxNextIndex && $value < $min) {
                $continueFlag = true;
            }

            if ($maxFirst <= $maxNext && $min !== null && $minIndex < $maxNextIndex) {
                if ($maxFirst > $maxNext) {
                    $currentDepth = $maxNext - $min;
                } else {
                    $currentDepth = $maxFirst - $min;
                }
                if ($depth < $currentDepth) {
                    $depth = $currentDepth;
                    $depthIndex = $minIndex;
                }
                $maxFirst = $maxNext;
                $maxFirstIndex = $maxNextIndex;
                $maxNext = null;
                $maxNextIndex = null;
                $min = null;
                $minIndex = null;
                $continueFlag = false;
            }

            $index++;
        }

        if ($maxNext !== null && $maxNext < $maxFirst && $minIndex !== null) {
            $currentDepth = $maxNext - $min;
            if ($depth == 0 || $depth < $currentDepth) {
                $depth = $currentDepth;
                $depthIndex = $minIndex;
            }
        }
    } while ($continueFlag == true);

    return ['depth' => $depth, 'index' => $depthIndex];
}

/**
 * @param array $data
 * @return int
 */
function maxLakeDepthArray(array $data): int
{
    $continueFlag = false;
    $index = 0;
    $depth = 0;
    $depthIndex = null;
    $maxNext = null;
    $maxNextIndex = null;
    do {
        $maxFirst = null;
        $maxFirstIndex = null;
        if ($continueFlag) {
            $index = $maxNextIndex+1;
            $continueFlag = false;
            $maxFirst = $maxNext;
            $maxFirstIndex = $maxNextIndex;
            $maxNext = null;
            $maxNextIndex = null;
        }
        $min = null;
        $minIndex = null;
        $minNext = null;
        $minNextIndex = null;

        do {
            $value = $data[$index];

            if ($maxFirst === null) {
                $maxFirst = $value;
                $maxFirstIndex = $index;
            } elseif ($maxFirst <= $value && $maxFirstIndex == $index - 1) {
                $maxFirst = $value;
                $maxFirstIndex = $index;
            } elseif (($maxFirst > $value && $min === null) || ($min > $value && $maxNext == null)) {
                $min = $value;
                $minIndex = $index;
            } elseif ($min > $value && ($minNext === null || $minNext > $value)) {
                $minNext = $value;
                $minNextIndex = $index;
            } elseif ($min < $value && ($maxNext === null || $maxNext <= $value)) {
                $maxNext = $value;
                $maxNextIndex = $index;
                if ($minNext !== null) {
                    $min = $minNext;
                    $minIndex = $minNextIndex;
                    $minNext = null;
                    $minNextIndex = null;
                }
                $continueFlag = false;
            }

            if ($continueFlag === false && $index > $maxNextIndex && $value < $min) {
                $continueFlag = true;
            }

            if ($maxFirst <= $maxNext && $min !== null && $minIndex < $maxNextIndex) {
                if ($maxFirst > $maxNext) {
                    $currentDepth = $maxNext - $min;
                } else {
                    $currentDepth = $maxFirst - $min;
                }
                if ($depth < $currentDepth) {
                    $depth = $currentDepth;
                    $depthIndex = $minIndex;
                }
                $maxFirst = $maxNext;
                $maxFirstIndex = $maxNextIndex;
                $maxNext = null;
                $maxNextIndex = null;
                $min = null;
                $minIndex = null;
                $continueFlag = false;
            }

            $index++;
        } while (isset($data[$index]));

        if ($maxNext !== null && $maxNext < $maxFirst && $minIndex !== null) {
            $currentDepth = $maxNext - $min;
            if ($depth == 0 || $depth < $currentDepth) {
                $depth = $currentDepth;
                $depthIndex = $minIndex;
            }
        }
    } while ($continueFlag == true);
    
    return $depth;
}

/**
 * @param array $data
 * @return int|null
 */
function maxLakeDepthArrayToFileWrap(array $data): ?int
{
    $fname = 'data.tmp';
    $file = fopen($fname, 'w');
    foreach ($data as $value) {
        fputs($file, $value.PHP_EOL);
    }
    fclose($file);

    $file = fopen($fname, 'r');
    $result = maxLakeDepthFile($file);
    fclose($file);
    unlink($fname);

    return isset($result['depth']) ? : null;
}
